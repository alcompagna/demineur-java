import java.util.EnumMap;

class Cellule {
    private Etat state;
    private boolean isMined;
    private int value;
    private EnumMap<Direction, Cellule> neighbors;

    Cellule() {
        this.state = Etat.IS_COVERED;
        this.isMined = false;
        this.value = 0;
        this.neighbors = new EnumMap<>(Direction.class);
    }


    /*--------------------------------------------*/
    /* Découverte de la cellule et de ses voisins */
    /*--------------------------------------------*/


    public boolean uncover() {
        if(this.isMined()) {
            this.state = Etat.IS_UNCOVERED;
            return false; // la cellule est minée
        }
        else if(!this.isMarked() && this.isCovered()) {
            this.state = Etat.IS_UNCOVERED;
            if(this.getValue() == 0) {
                uncoverNeighbors();
            }
            return true;
        }
        else{
            return true; // aucune mine n'a été découverte
        }
    }

    /*-----------*/
    /* Affichage */
    /*-----------*/

    public String toString() {
        String res = "";
        String reset = "\u001B[0m"; // couleur de base
        String red = "\u001B[31m";
        String blue = "\u001B[34m";
        String green = "\u001B[32m";
        String yellow = "\u001B[33m";

        if (this.isMarked()) {
            res += blue + " " + "\u2691" + reset;
        } else if (this.isCovered()) {
            res += "\u2B1C";
        } else {
            int nbMinesVoisines = this.getValue();
            if (this.isMined()) {
                res += red + " " + "\u2620" + reset;
            } else if (nbMinesVoisines == 0) {
                res += green + " " + "\u25A1" + reset;
            } else { 
                res += yellow + " " + nbMinesVoisines + reset;
            }
        }
        res += " ";

        return res;
    }


    private void uncoverNeighbors() {

        for(Direction dir : this.neighbors.keySet()) {
            Cellule cell = this.neighbors.get(dir);
            cell.uncover();
        }
    }

    /*-------------------------------------------*/
    /* Action et getter sur l'état de la cellule */
    /*-------------------------------------------*/


    // Opérations sur l'état de couverture de la cellule

    public boolean isUncovered() {
        return this.state.equals(Etat.IS_UNCOVERED);
    }


    public boolean isCovered() {
        return this.state.equals(Etat.IS_COVERED);
    }


    // Opérations sur la valeur de la cellule 


    public int getValue() {
        return this.value;
    }


    public void setValue(int nb) {
        this.value = nb;
    }


    private void increaseValueByOne() {
        this.value += 1; 
    }


    // Opérations sur l'état miné de la cellule 

    public void setMine(boolean mine) {
        this.isMined = mine;
    }


    public boolean isMined(){
        return this.isMined;
    }


    // Opérations sur l'état marqué de la cellule

    public void toggleMark(Grille grid){
        if(this.state.equals(Etat.IS_MARKED)){
            this.state = Etat.IS_COVERED;
            grid.removeFlag(this);
            // System.out.println("flag removed");
        }
        else if(this.state.equals(Etat.IS_COVERED)) {
            if(grid.addFlag(this)) { // si on peut ajouter un  drapeau
                this.state = Etat.IS_MARKED;
                // System.out.println("flag added");
            }
       }
    }

    public boolean isMarked(){
        return this.state.equals(Etat.IS_MARKED);
    }

    
    // Opérations sur les voisins de la cellule

    public  Cellule getNeighbor(Direction dir) {
        return this.neighbors.get(dir);
    }


    public void setNeighbor(Direction dir, Cellule neighbor){
        this.neighbors.put(dir, neighbor);
    }


    public void increaseNeighborsValue() {
        
        for(Direction dir : this.neighbors.keySet()) {
            Cellule cell  = this.neighbors.get(dir);
            cell.increaseValueByOne();         
        }
    }


    // Opération sur le status global de la cellule

    protected Etat getState() {
        return this.state;
    }


    protected void setState(Etat state) {
        this.state = state;
    }
}