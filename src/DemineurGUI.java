import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.*;


class DemineurGUI extends JFrame {

    private int nbLignes;
    private int nbColonnes;
    private Jeu jeu;
    private JButton[][] boutonsCases;
    public static final Color COLOR_UNCOVERED = new Color(219,214,212);
    public static final Color COLOR_COVERED = new Color(156,149,149);
    public static final Color BORDER_COLOR = new Color(76,59,59);
    public static final Color FONT_COLOR = new Color(0,0,0);



    DemineurGUI(Jeu jeu, int nbLignes, int nbColonnes) {
        super("Démineur");
        this.jeu = jeu;
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        

        JPanel panel = new JPanel(new GridLayout(nbLignes, nbColonnes));
        JPanel sidePanel = new JPanel(new GridLayout(2, 1));


        JButton quitButton = new JButton("Quitter");
        quitButton.setBackground(COLOR_COVERED);
        quitButton.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));

        sidePanel.add(quitButton);

        quitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON1) { quitter(); }
            }
        });

        // Bouton pour sauvegarder

        JButton saveButton = new JButton("Sauvegarder");
        saveButton.setBackground(COLOR_COVERED);
        saveButton.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        sidePanel.add(saveButton);

        saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON1) { save(); }
            }
        });


        // Ajout des bandes pour les drapeaux sur le coté

        JPanel nbDrapeauxPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel EmojiDrapeauLabel = new JLabel("\u2691");
        JLabel nbDrapeauxLabel = new JLabel(this.jeu.getGrid().getNbFlags() + " / " + this.jeu.getGrid().getNbMines());
        Font emojiFont = new Font("Segeo UI Emoji", Font.PLAIN, 48);

        EmojiDrapeauLabel.setFont(emojiFont);
        nbDrapeauxPanel.add(EmojiDrapeauLabel);
        nbDrapeauxPanel.add(nbDrapeauxLabel);
        sidePanel.add(nbDrapeauxPanel);

        
        // Ajout des cases du démineur

        boutonsCases = new JButton[nbLignes][nbColonnes];

        for (int i = 0; i < nbLignes; i++) {
            for (int j = 0; j < nbColonnes; j++) {
                JButton bouton = new JButton();
                bouton.setBackground(COLOR_COVERED);
                bouton.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
                bouton.setForeground(FONT_COLOR);
                bouton.setPreferredSize(new Dimension(70, 70));
                bouton.setComponentPopupMenu(null);
                boutonsCases[i][j] = bouton;   
                panel.add(bouton);


                // Écouteur de clic sur les boutons du jeu
                boutonsCases[i][j].addMouseListener(new MouseAdapter() {

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getButton() == MouseEvent.BUTTON1) {

                            int ligne = -1;
                            int colonne = -1;

                            // Trouver la position du bouton cliqué
                            for (int i = 0; i < nbLignes; i++) {
                                for (int j = 0; j < nbColonnes; j++) {
                                    if (e.getSource() == boutonsCases[i][j]) { // Si le bouton cliqué est le courant
                                        ligne = i;
                                        colonne = j;
                                        break;
                                    }
                                }
                            }

                            // Si la position du bouton a été trouvée, appeler la méthode correspondante
                            // dans la classe Jeu
                            if (ligne != -1 && colonne != -1) {
                                Etat state = DemineurGUI.this.jeu.getCellState(ligne, colonne);
                                boolean res = false;

                                if (state != Etat.IS_MARKED) {
                                    res = DemineurGUI.this.jeu.jouerCellule(ligne, colonne, "u");

                                }

                                boutonsCases[ligne][colonne].setFont(new Font("Arial", Font.PLAIN, 60));
                                
                                if (res) {
                                    if (DemineurGUI.this.jeu.getCell(ligne, colonne).isMined()) {
                                        boutonsCases[ligne][colonne].setText("\u2620");
                                        boutonsCases[ligne][colonne].setFont(new Font("Arial", Font.PLAIN, 60));
                                        DemineurGUI.this.jeu.gameOver();
                                    } else {
                                        int val = DemineurGUI.this.jeu.getCellValue(ligne, colonne);
                                        boutonsCases[ligne][colonne].setText("" + val);
                                        boutonsCases[ligne][colonne].setFont(new Font("Arial", Font.PLAIN, 40));
                                    }

                                    update(); // update l'affichage
                                }
                            }

                        } else if (e.getButton() == MouseEvent.BUTTON3) {

                            int ligne = -1;
                            int colonne = -1;

                            // Trouver la position du bouton cliqué
                            for (int i = 0; i < nbLignes; i++) {
                                for (int j = 0; j < nbColonnes; j++) {
                                    if (e.getSource() == boutonsCases[i][j]) { // Si le bouton cliqué est le courant
                                        ligne = i;
                                        colonne = j;
                                        break;
                                    }
                                }
                            }

                            if (ligne != -1 && colonne != -1) {
                                boolean res = DemineurGUI.this.jeu.jouerCellule(ligne, colonne, "m");

                                if (res) {
                                    Etat state = DemineurGUI.this.jeu.getCellState(ligne, colonne);

                                    if (state != Etat.IS_UNCOVERED) {
                                        if (state == Etat.IS_MARKED) {
                                            boutonsCases[ligne][colonne].setText("\u2691");
                                            boutonsCases[ligne][colonne].setFont(new Font("Arial", Font.PLAIN, 60));
                                        } else {
                                            boutonsCases[ligne][colonne].setText("");
                                        }
                                    }
                                }
                            }
                        }

                        nbDrapeauxLabel.setText(DemineurGUI.this.jeu.getGrid().getNbFlags() + " / " + DemineurGUI.this.jeu.getGrid().getNbMines());

                        if (DemineurGUI.this.jeu.isGameOver()) {
                            DemineurGUI.this.jeu.getGrid().uncoverAll();
                            update();
                            JOptionPane.showMessageDialog(null, "Vous avez perdu ! Game over ");
                            Jeu.resetSave();
                            quitter();
                        } else if (DemineurGUI.this.jeu.isGameWon()) {
                            DemineurGUI.this.jeu.getGrid().uncoverAll();
                            update();
                            JOptionPane.showMessageDialog(null, "Félicitations ! Vous avez gagné ! ");
                            Jeu.resetSave();
                            quitter();
                        }
                    }
                });
                
            }
        }

        update();
        nbDrapeauxLabel.setText(this.jeu.getGrid().getNbFlags() + " / " + this.jeu.getGrid().getNbMines());

        add(sidePanel, BorderLayout.SOUTH);
        add(panel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

    }

    private void update() {
        for (int i = 0; i < nbLignes; i++) {
            for (int j = 0; j < nbColonnes; j++) {
                if (DemineurGUI.this.jeu.getCellState(i, j) == Etat.IS_UNCOVERED) {
                    int val = DemineurGUI.this.jeu.getCellValue(i, j);
                    boutonsCases[i][j].setText("" + val);
                    switch (val){
                        case 0:
                            boutonsCases[i][j].setText("");
                            boutonsCases[i][j].setBackground(COLOR_UNCOVERED);
                            break;
                        case 1:
                            boutonsCases[i][j].setBackground(new Color(56, 255, 94));
                            break;
                        case 2:
                            boutonsCases[i][j].setBackground(new Color(117, 255, 56));
                            break;
                        case 3:
                            boutonsCases[i][j].setBackground(new Color(217, 255, 56));
                            break;
                        case 4:
                            boutonsCases[i][j].setBackground(new Color(255, 194, 56));
                            break;
                        
                        default:
                            boutonsCases[i][j].setBackground(new Color(255, 94, 56));
                    }

                    boutonsCases[i][j].setFont(new Font("Arial", Font.PLAIN, 40));
                    if(DemineurGUI.this.jeu.getCell(i, j).isMined()) { 
                        boutonsCases[i][j].setText("\u2620"); 
                        boutonsCases[i][j].setBackground(COLOR_UNCOVERED);
                    }
                }
            }
        }
    }

    private void quitter() {
        setVisible(false);
        dispose();
    }

    private void save() {
        try {
            Path filePath = Paths.get("sauvegarde.txt");
            this.jeu.getGrid().save(filePath);
            JOptionPane.showMessageDialog(null, "Sauvegarde réussie");
        } catch (IOException e) {
            System.out.println("Erreur lors de la sauvegarde du fichier : " + e.getMessage());
            e.printStackTrace();
        }
    }

}
