import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Jeu jeu=null;
        Path filePath = Paths.get("sauvegarde.txt");
        Scanner scanner = null;
        String choix = "";
        Boolean boucle = true;
        String mode;

        if(args.length == 1) {
            mode = args[0];
        }
        else { 
            throw new IllegalArgumentException("exactly one mode is taken by this game ('g' or 't')");
        }
        
        try{

            scanner = new Scanner(System.in);
            while(boucle){

                while(!choix.equals("1") && !choix.equals("2") && !choix.equals("3")){

                    System.out.println("\033[2J");
                    System.out.println("╔══════════════════════════╗");
                    System.out.println("║         DEMINEUR         ║");
                    System.out.println("╠══════════════════════════╣");
                    System.out.println("║ 1. Nouvelle partie       ║");
                    System.out.println("║ 2. Charger partie        ║");
                    System.out.println("║ 3. Quitter               ║");
                    System.out.println("╚══════════════════════════╝");

                    
                    choix = scanner.next();

                    switch (choix) {
                        case "1":
                            jeu = new Jeu(scanner, mode);
                            break;
                        
                        case "2":
                            if (filePath.toFile().length()==0){
                                System.out.println("Pas de partie existante, nouvelle partie :");
                                jeu = new Jeu(scanner, mode);
                            }
                            else{
                                jeu = new Jeu(scanner,filePath, mode);
                            }
                            break;
                        case "3":
                            System.exit(0);
                        default:
                            System.out.println("Veuillez saisir une valeur correcte !");
                            break;
                    }
                }

                jeu.jouerPartie();
                choix="";

            }
        }
        finally{
            if (scanner!=null){scanner.close();}
        }
    }


}
