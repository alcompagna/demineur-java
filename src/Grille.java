import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Random;
import java.util.Scanner;

class Grille {
    private Cellule[][] grid;
    private int nbMines;
    private int nb_colonnes;
    private int nb_lignes;
    private int nbFoundedMines = 0;
    private boolean isGameOver = false;
    private int nbFlags = 0;


    Grille(int nb_lignes, int nb_colonnes, int pourcentageMines) {

        setSize(nb_colonnes, nb_lignes);
        createGrid();
        setMines(pourcentageMines);

        // Init de la grille avec des cellules basique qui ne connaissent ni leur état
        // ni leurs voisins

        initGrid();

        // Init des voisins de chaque cellule

        initNeighbors();

        // Init des mines sur qlq mines aléatoirement

        generateRandomMines();
    }


    /* Méthodes utiles pour le constructeur */

    private void setSize(int nb_colonnes, int nb_lignes) {
        if (nb_lignes > 0 && nb_lignes <= 20 && nb_colonnes > 0 && nb_colonnes <= 20) {
            this.nb_lignes = nb_lignes;
            this.nb_colonnes = nb_colonnes;
        } else {
            throw new IllegalArgumentException("Number of rows and columns must be at minimun 1 and at maximum 20");
        }
    }


    private void createGrid() {
        this.grid = new Cellule[this.nb_lignes][this.nb_colonnes];
    }


    private void setMines(int pourcentageMines) {
        if (pourcentageMines >= 0 && pourcentageMines <= 100) {
            int nbMines = this.nb_colonnes * this.nb_lignes * pourcentageMines / 100;
            if (nbMines == 0) {
                this.nbMines = 1;
            } else {
                this.nbMines = nbMines;
            }
        } else {
            throw new IllegalArgumentException("Percentage of mines must be between 0 and 100%");
        }
    }
    

    private void initGrid() {

        for (int i = 0; i < this.nb_lignes; i++) {
            for (int j = 0; j < this.nb_colonnes; j++) {
                this.grid[i][j] = new Cellule();
            }
        }
    }


    private void initNeighbors() {

        int i, j;

        for (i = 0; i < this.nb_lignes; i++) {
            for (j = 0; j < this.nb_colonnes; j++) {
                for (Direction dir : Direction.values()) {
                    switch(dir) {
                        case UP:
                            if(isInGrid(i-1, j)) { this.grid[i][j].setNeighbor(dir, this.grid[i - 1][j]); }
                            break;
                        case DOWN:
                            if(isInGrid(i+1, j)) { this.grid[i][j].setNeighbor(dir, this.grid[i + 1][j]); } 
                            break;
                        case LEFT:
                            if(isInGrid(i, j-1)) { this.grid[i][j].setNeighbor(dir, this.grid[i][j - 1]); } 
                            break;
                        case RIGHT:
                            if(isInGrid(i, j+1)) { this.grid[i][j].setNeighbor(dir, this.grid[i][j + 1]); } 
                            break;
                        case UP_LEFT:
                            if(isInGrid(i-1, j-1)) { this.grid[i][j].setNeighbor(dir, this.grid[i - 1][j - 1]); } 
                            break;
                        case UP_RIGHT:
                            if(isInGrid(i-1, j+1)) { this.grid[i][j].setNeighbor(dir, this.grid[i - 1][j + 1]); } 
                            break;
                        case DOWN_RIGHT:
                            if(isInGrid(i+1, j+1)) { this.grid[i][j].setNeighbor(dir, this.grid[i + 1][j + 1]); } 
                            break;
                        case DOWN_LEFT:
                            if(isInGrid(i+1, j-1)) { this.grid[i][j].setNeighbor(dir, this.grid[i + 1][j - 1]); }  
                            break;
                    }
                }
            }
        }
    }


    protected boolean isInGrid(int y, int x) {
        return y >= 0 && y < this.grid.length && x >= 0 && x < this.grid[0].length;
    }


    private void generateRandomMines() {
        int n = 0;
        Random random = new Random();

        int x = random.nextInt(this.grid[0].length);
        int y = random.nextInt(this.grid.length);

        while (n < this.nbMines) {
            if (!this.grid[y][x].isMined()) { // Si la celleule ne contient pas deja de mine
                this.grid[y][x].setMine(true); // on ajoute une mine sur la case
                this.grid[y][x].increaseNeighborsValue();
                n++;
            }
            x = random.nextInt(this.grid[0].length);
            y = random.nextInt(this.grid.length);
        }
    }


    @Override
    public String toString() {
        String res = "  ";

        res += "\033[2J"; // efface le terminal
        for(int i=0; i<this.getNbColonnes(); i++) {
            if(i<9){
                res+= " " + (i+1) + " ";
            }
            else{
                res += (i+1) + " ";
            }
        }

        res += "\n";

        for(int i=0; i < this.nb_lignes; i++) {
            if(i+1 < 10) {
                res += " " + (i+1);
            }
            else{
                res += (i+1);
            }
            for(int j=0; j < this.nb_colonnes; j++) {
                res += grid[i][j].toString();
            }
            res += "\n";
        }

        return res;
    }


    /* Getters */

    public int getNbLignes(){
        return this.nb_lignes;
    }


    public int getNbColonnes(){
        return this.nb_colonnes;
    }


    public Cellule getCellule(int x, int y) {
        return this.grid[x][y];
    }


    /* gestion des drapeaux ajoutés et du compte des mines trouvées*/

    private void addFoundedMine() {
        this.nbFoundedMines++;
    }


    private void removeFoundedMine() {
        this.nbFoundedMines--;
    }


    private void addNbMines() {
        this.nbMines++;
    }


    protected boolean addFlag(Cellule cell) {
        if (this.nbFlags < this.nbMines) {
            this.nbFlags++;
            if (cell.isMined()) {
                addFoundedMine();
            }
            return true;
        }

        return false;
    }


    protected void removeFlag(Cellule cell) {
        this.nbFlags--;
        if (cell.isMined()) {
            removeFoundedMine();
        }
    }


    /* Tests pour la fin de partie */

    public boolean isGameWon() {
        return this.nbMines == this.nbFoundedMines;
    }

    public void gameOver() {
        this.isGameOver = true;
    }

    public boolean isGameOver() {
        return this.isGameOver;
    }


    // gère la découverte de toute la grille en fin de partie
    public void uncoverAll() {
        int i, j;

        for (i = 0; i < this.nb_lignes; i++) {
            for (j = 0; j < this.nb_colonnes; j++) {
                if (grid[i][j].getState() == Etat.IS_COVERED) {
                    grid[i][j].uncover();
                }
            }
        }
    }


    /* Méthodes de sauvegarde et de chargement */

    public void save(Path filePath) throws IOException {
        try (PrintWriter writer = new PrintWriter(filePath.toFile())) {
            // Écriture des dimensions de la grille
            writer.println(this.nb_lignes);
            writer.println(this.nb_colonnes);

            // Écriture de l'état de chaque case
            for (int i = 0; i < this.nb_lignes; i++) {
                for (int j = 0; j < this.nb_colonnes; j++) {
                    Cellule cell = this.grid[i][j];
                    // Coordonnées i,j + Etat de la cellule (IS_COVERED/IS_UNCOVERED/IS_MARKED) +
                    // est une mine ? (true/false)
                    writer.println(i + "," + j + "," + cell.getState() + "," + cell.isMined() + "," + cell.getValue());
                }
            }
        }
    }


    public void load(Path filePath) throws IOException {
        try (Scanner scanner = new Scanner(filePath)) {
            // Lecture des dimensions de la grille
            int loadedRows = scanner.nextInt();
            int loadedCols = scanner.nextInt();
            scanner.nextLine(); // Lire la fin de la ligne

            
            // Initialisation de la grille
            this.nbMines = 0;
            setSize(loadedRows, loadedCols);
            createGrid();
            initGrid();

            while (scanner.hasNextLine()) {
                String[] tokens = scanner.nextLine().split(",");
                int i = Integer.parseInt(tokens[0]);
                int j = Integer.parseInt(tokens[1]);
                Cellule cell = this.grid[i][j];
                
                // valeur du boolean isMined
                cell.setMine(Boolean.parseBoolean(tokens[3])); // parseBolean permet de transformer la chaine de
                                                               // caractères en boolean

                // ajout de l'etat de la cellule
                cell.setState(Etat.valueOf(tokens[2])); // Etat.valueOf() permet de transformer la chaine de caractères en instance de Etat
                if(cell.getState() == Etat.IS_MARKED) { addFlag(cell); }

                if (Boolean.parseBoolean(tokens[3])) {
                    addNbMines();
                    
                }

                cell.setValue(Integer.parseInt(tokens[4]));
            }
            initNeighbors();
        }
    }
    

    protected int getNbFlags() {
        return this.nbFlags;
    }


    protected int getNbMines() {
        return this.nbMines;
    }

}
