import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Jeu {
    private Grille grid;
    private Scanner scanner = new Scanner(System.in);
    private DemineurGUI gui;
    private DemineurTUI tui;

    // nouvelle partie

    Jeu(Scanner scanner, String mode) {

        // gerer les erreurs d'entrée avec verif

        this.scanner = scanner;
        int nb_lignes = this.verif( 1, 20, "Entrer le nombre de lignes (q pour quitter):");

        if(nb_lignes == -2) {quitter();}
        
        int nb_colonnes = this.verif( 1, 20, "Entrer le nombre de colonnes (q pour quitter):");

        if(nb_lignes == -2) {quitter();}

        int percentage = this.verif( 1, 100, "Entrer un pourcentage de mines (ex: 20) (q pour quitter):");

        if(percentage == -2) {quitter();}

        // crée la grille
        this.grid = new Grille(nb_lignes, nb_colonnes, percentage);

        //mode texte
        if(mode.equals("t")) {
            this.tui = new DemineurTUI(nb_lignes, nb_colonnes, this.grid);
        }
        else if (mode.equals("g")){
           this.gui = new DemineurGUI(this, nb_lignes, nb_colonnes);
        }
        else {
            throw new IllegalArgumentException("mode must be 'g' for GUI or 't' for TUI");
        }
        
    }


    // charger partie

    Jeu(Scanner scanner,Path filePath, String mode) {
        this.grid = new Grille(1, 1, 1); // init d'une grille sur laquelle s'appuyer
        this.scanner=scanner;
        try {
            grid.load(filePath);
            if(mode.equals("t")) {
                this.tui = new DemineurTUI(grid.getNbLignes(), grid.getNbColonnes(), this.grid);
            }
            else if (mode.equals("g")){
               this.gui = new DemineurGUI(this, grid.getNbLignes(), grid.getNbColonnes());
            }
            else {
                throw new IllegalArgumentException("mode must be 'g' for GUI or 't' for TUI");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean jouerPartie() {
        if(this.tui != null) { tui.jouerPartie(); }
        return true;
    }


    protected static void resetSave(){

        File file = new File("sauvegarde.txt");
        
        try{
            file.delete();
            file.createNewFile();
        }
        catch(IOException e){
            System.out.println("Error : reset failed");
            e.printStackTrace();
        }
    }

    

    private void quitter(){
        System.out.println("Au revoir !");
        System.exit(0);
    }


    protected boolean jouerCellule(int row, int col, String action) {

        switch (action) {
            case "u":
                boolean isNotMined = this.grid.getCellule(row, col).uncover();

                if (isNotMined == false) { // si c'est une mine
                    this.grid.gameOver();
                }
                break;
            case "m":
                this.grid.getCellule(row, col).toggleMark(this.grid);
                break;
            case "q":
                System.out.println("Au revoir !");
                System.exit(0);
                break;
            case "s":
                try {
                    Path filePath = Paths.get("sauvegarde.txt");
                    this.grid.save(filePath);
                } catch (IOException e) {
                    System.out.println("Erreur lors de la sauvegarde du fichier : " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Entrer une valeur correcte !");
                return false;
        }
        return true;
    }


    protected int getCellValue(int row, int col) {
        return this.grid.getCellule(row, col).getValue();
    }


    protected Etat getCellState(int row, int col) {
        return this.grid.getCellule(row, col).getState();
    }


    protected Cellule getCell(int row, int col) {
        return this.grid.getCellule(row, col);
    }


    protected boolean isInGrid(int row, int col) {
        return this.grid.isInGrid(row, col);
    }


    protected boolean isGameOver(){
        return this.grid.isGameOver();
    }

    protected void gameOver() {
        this.grid.gameOver();
    }

    protected boolean isGameWon() {
        return this.grid.isGameWon();
    }


    private int verif(int borneInf, int borneSup, String message){

        int valeur = -1;
        System.out.println(message);

        do {
            if (scanner.hasNextInt()){ 
                valeur = scanner.nextInt();
                if (valeur < borneInf || valeur > borneSup){
                    System.out.println("Entrer un entier entre " + borneInf + " et " + borneSup + " (compris) :");
                    valeur = -1;
                }
            }
            else {
                String s = scanner.next();
                if (s.equals("q")){
                    return -2;
                }
                else{
                    System.out.println("Entrer un entier (q pour quitter):");
                    if (s.equals("q")){
                        return -2;
                    }
                }
            }
        } while (valeur == -1);

    return valeur;
    }

    protected Grille getGrid(){
        return this.grid;
    }

}
