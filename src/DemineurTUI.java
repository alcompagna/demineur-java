import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

class DemineurTUI {

    private int nbLignes;
    private int nbColonnes;
    private Grille grid;
    private Scanner scanner = new Scanner(System.in);

    DemineurTUI(int nbLignes, int nbColonnes, Grille grid) {
        this.grid = grid;
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
    }

    public boolean jouerPartie() {
        int row,col;
        boolean boucle=true;

        try{

            while (!grid.isGameWon() && !grid.isGameOver() && boucle) { // tant que la partie n'est pas finie

                System.out.println(grid);

                row = this.verif(1,nbLignes,"Entrer la ligne (q pour quitter):");
                if (row==-2){
                    boucle = false;
                    break;
                }
                col = this.verif(1,nbColonnes,"Entrer la colonne (q pour quitter):");
                if (col==-2){
                    boucle=false;
                    break;
                }



                System.out.println("Entrer 'u' pour découvrir, 'm' pour marquer,'s' sauvegarder. (q pour quitter) :");
                String action = scanner.next();

                switch (action) {
                    case "u":
                        boolean isNotMined = grid.getCellule(row-1, col-1).uncover();

                    if (isNotMined == false) { // si c'est une mine
                        grid.gameOver();
                    }
                    break;
                        case "m":
                        grid.getCellule(row-1, col-1).toggleMark(grid);
                        break;
                    case "q":
                        System.out.println("Au revoir !");
                        boucle=false;
                        break;
                    case "s":
                        try {
                            Path filePath = Paths.get("sauvegarde.txt");
                            grid.save(filePath);
                        } catch (IOException e) {
                            System.out.println("Erreur lors de la sauvegarde du fichier : " + e.getMessage());
                            e.printStackTrace();
                        }
                        break;
                    default:
                        System.out.println("Entrer une valeur correcte !");
                        break;
                }
            }

            // fin du jeu

            if (grid.isGameWon()) {
                System.out.println(grid);
                System.out.println("Bravo, tu as gagné !");
                Jeu.resetSave();
            } else if(boucle==true){
                grid.uncoverAll();
                System.out.println(grid);
                System.out.println("Tu as perdu, Game Over.");
                Jeu.resetSave();
            }
            Thread.sleep(1000);

        }catch(Exception e){
            e.printStackTrace();
        }

        return true;
    }


    private int verif(int borneInf, int borneSup, String message){

        int valeur = -1;
        System.out.println(message);

        do {
            if (scanner.hasNextInt()){ 
                valeur = scanner.nextInt();
                if (valeur < borneInf || valeur > borneSup){
                    System.out.println("Entrer un entier entre " + borneInf + " et " + borneSup + " (compris) :");
                    valeur = -1;
                }
            }
            else {
                String s = scanner.next();
                if (s.equals("q")){
                    return -2;
                }
                else{
                    System.out.println("Entrer un entier (q pour quitter):");
                    if (s.equals("q")){
                        return -2;
                    }
                }
            }
        } while (valeur == -1);

    return valeur;
    }
    
}
