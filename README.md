## Démineur Java
Projet scolaire de L2 Informatique dans lequel nous devions, en binôme, recréer le jeu du démineur en Java.

## Description
Nous avons codé le jeu du démineur en Java afin d'appréhender les différents principes de conception architecturelle qui sont liés à la prorammation orientée objet. Pour jouer il suffit de lancer en ligne de commande **java Main g** pour jouer sur l'interface graphique et **java Main t** pour l'interface textuelle dans le terminal. *Main.class* se trouve dans le dossier build.

## Auteurs
Ce projet a été effectué en collaboration avec Zakaria BERRAHAL, un camarade de classe.

